import imei_utils

random_imei = imei_utils.generate()

print "Random IMEI:", random_imei

print "Check digit:", imei_utils.calculate_check_digit(random_imei)

print "Is valid   :", imei_utils.validate(random_imei)
