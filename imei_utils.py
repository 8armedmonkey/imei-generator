import random

# Reporting Body Identifier (RBI) list: http://en.wikipedia.org/wiki/Reporting_Body_Identifier
RBI = ["01", "10", "30", "33", "35", "44", "45", "49", "50", "51", "52", "53", "54", "86", "91", "98", "99"]

IMEI_LENGTH = 15


def generate():
    digits = ["0"] * IMEI_LENGTH

    r = random.Random()
    r.seed()

    random_rbi = RBI[r.randint(0, len(RBI) - 1)]
    index = 0

    digits[index] = random_rbi[0:1]
    index += 1

    digits[index] = random_rbi[1:2]
    index += 1

    while index < IMEI_LENGTH - 1:
        digits[index] = str(r.randint(0, 9))
        index += 1

    length_offset = (IMEI_LENGTH + 1) % 2
    total_sum = 0

    for i in range(0, IMEI_LENGTH):
        if (i + length_offset) % 2 == 1:
            t = int(digits[i]) * 2

            if t > 9:
                t -= 9

            total_sum += t
        else:
            total_sum += int(digits[i])

    final_digit = (10 - (total_sum % 10)) % 10
    digits[IMEI_LENGTH - 1] = str(final_digit)

    return "".join(digits)


def calculate_check_digit(input_imei):
    total_sum = 0

    for i in range(0, len(input_imei) - 1):
        if i % 2 == 1:
            t = str(2 * int(input_imei[i]))

            if len(t) == 2:
                total_sum += (int(t[0:1]) + int(t[1:2]))
            else:
                total_sum += int(t)
        else:
            total_sum += int(input_imei[i])

    if total_sum % 10 == 0:
        number = total_sum
    else:
        number = (total_sum / 10 + 1) * 10

    return number - total_sum


def validate(input_imei):
    if len(input_imei) != IMEI_LENGTH:
        return False

    final_digit = int(input_imei[-1])

    return calculate_check_digit(input_imei) == final_digit
